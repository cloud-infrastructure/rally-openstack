# Copyright 2018: Red Hat Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from rally.common import logging
from rally.task import types
from rally.task import validation

from rally_openstack.common import consts
from rally_openstack.task import scenario
from rally_openstack.task.scenarios.octavia import full_lb as full_lb


"""Scenarios for Octavia Loadbalancer."""

LOG = logging.getLogger(__name__)


@types.convert(image={"type": "glance_image"},
               flavor={"type": "nova_flavor"})
@validation.add("image_valid_on_flavor", flavor_param="flavor",
                image_param="image", fail_on_404_image=False)
@validation.add("required_services", services=[consts.Service.NOVA,
                                               consts.Service.CINDER,
                                               consts.Service.OCTAVIA])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["nova", "octavia"],
                             "keypair@openstack": {},
                             "allow_ssh@openstack": None},
                    name="Octavia.cern_create_delete_l7_lb",
                    platform="openstack")
class CreateFailoverL7LB(full_lb.BaseFullLB):

    def _populate_lb_l7(self, loadbalancers, listener_args,
                        l7policy_args, l7rule_args):
        """Populate a loadbalancer with listener, pool and members.

        """
        for lb in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(lb)
            listener = self.octavia.listener_create(json={
                "listener": dict(
                    loadbalancer_id=lb["id"],
                    **listener_args
                )})

            self.octavia.wait_for_loadbalancer_prov_status(lb)
            l7policy = self.octavia.l7policy_create(json={
                "l7policy": dict(
                    listener_id=listener["listener"]["id"],
                    **l7policy_args
                )})
            self.octavia.wait_for_loadbalancer_prov_status(lb)
            self.octavia.l7rule_create(
                l7policy_id=l7policy["l7policy"]["id"],
                json={
                    "rule": dict(
                        **l7rule_args)})

    def run(self, flavor, image,
            listener_args, l7policy_args, l7rule_args, test_command,
            member_args, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and test L7 policies on it.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        try:
            loadbalancers = self._create_load_balancer_every_flavor(
                admin_state=admin_state, **kwargs)
            self._populate_lb_l7(loadbalancers, listener_args,
                                 l7policy_args, l7rule_args)
            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)

            self._test_lb(loadbalancers, test_command,
                          flavor=flavor, image=image, **member_args)

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
                self.octavia.load_balancer_failover(loadbalancer["id"])

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)

            self._test_lb(loadbalancers, test_command,
                          flavor=flavor, image=image, **member_args)

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
                self.octavia.load_balancer_delete(
                    loadbalancer["id"], cascade=True)
        except Exception:
            raise
        finally:
            self._cleanup()
