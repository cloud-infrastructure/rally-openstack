# Copyright 2018: Red Hat Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from rally.common import logging
from rally.task import validation
from rally_openstack.common import consts
from rally_openstack.task import scenario
from rally_openstack.task.scenarios.octavia import utils as octavia_utils

LOG = logging.getLogger(__name__)

"""Scenarios for Octavia Loadbalancer."""


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_list_loadbalancers",
                    platform="openstack")
class CreateAndListLoadbalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)

        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
        self.octavia.load_balancer_list()


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_delete_loadbalancers",
                    platform="openstack")
class CreateAndDeleteLoadbalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)

        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_delete(
                loadbalancer["id"])


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_update_loadbalancers",
                    platform="openstack")
class CreateAndUpdateLoadBalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)

        for loadbalancer in loadbalancers:
            update_loadbalancer = {
                "name": self.generate_random_name()
            }
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_set(
                lb_id=loadbalancer["id"],
                lb_update_args=update_loadbalancer)


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_stats_loadbalancers",
                    platform="openstack")
class CreateAndShowStatsLoadBalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)
        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_stats_show(
                loadbalancer["id"])


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_show_loadbalancers",
                    platform="openstack")
class CreateAndShowLoadBalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)

        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_show(
                loadbalancer["id"])


@validation.add("required_services", services=[
    consts.Service.OCTAVIA, consts.Service.NEUTRON])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["octavia"]},
                    name="Octavia.create_and_failover_loadbalancers",
                    platform="openstack")
class CreateAndFailoverLoadBalancers(octavia_utils.OctaviaBase):

    def run(self, admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        loadbalancers = self._create_load_balancer_every_flavor(
            admin_state=admin_state, **kwargs)

        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_failover(
                loadbalancer["id"])

        for loadbalancer in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
            self.octavia.load_balancer_show(
                loadbalancer["id"])
