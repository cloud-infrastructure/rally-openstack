# Copyright 2018: Red Hat Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import io
import select
import shlex
import time

from rally.common import logging
from rally import exceptions
from rally.task import types
from rally.task import validation
from rally.utils import sshutils

from rally_openstack.common import consts
from rally_openstack.task import scenario
from rally_openstack.task.scenarios.octavia import utils as octavia_utils
from rally_openstack.task.scenarios.vm import utils as vm_utils


"""Scenarios for Octavia Loadbalancer."""

LOG = logging.getLogger(__name__)


class SSHAsync(sshutils.SSH):
    session = None

    def run_async(self, cmd):
        """Run a command asynchronously and return the command SSH session

        """
        if isinstance(cmd, (list, tuple)):
            cmd = " ".join(shlex.quote(str(p)) for p in cmd)

        stdin = io.StringIO(cmd)

        client = self._get_client()
        transport = client.get_transport()
        self.session = transport.open_session()
        self.session.exec_command("/bin/sh")

        data_to_send = None
        while True:
            if self.session.send_ready():
                if stdin is not None and not stdin.closed:
                    if not data_to_send:
                        data_to_send = stdin.read(4096)
                        if not data_to_send:
                            stdin.close()
                            self.session.shutdown_write()
                            break
                    sent_bytes = self.session.send(data_to_send)
                    LOG.debug("sent: %s" % data_to_send[:sent_bytes])
                    data_to_send = data_to_send[sent_bytes:]


class BaseFullLB(octavia_utils.OctaviaBase, vm_utils.VMScenario):
    _members = []

    def _run_command_async(self, image, flavor, port, command,
                           username, password, is_backend=True, **kwargs):
        """Create VM and run asynchronous task.

        """
        server, fip = self._boot_server_with_fip(image, flavor,
                                                 use_floating_ip=False,
                                                 **kwargs)

        try:
            self._wait_for_ping(fip["ip"])
            ssh = SSHAsync(username, fip["ip"], port=port,
                           password=password)

            try:
                self._wait_for_ssh(ssh)
                LOG.debug("Running command on %s: %s" % (fip["ip"], command))
                ssh.run_async(cmd=command)

                if is_backend:
                    self._members.append((fip["ip"], ssh))
                return ssh
            except exceptions.SSHTimeout:
                ssh.close()
                console_logs = self._get_server_console_output(server)
                LOG.info("VM console logs:\n%s" % console_logs)
                raise
        except (exceptions.TimeoutException,
                exceptions.SSHTimeout):
            console_logs = self._get_server_console_output(server)
            LOG.info("VM console logs:\n%s" % console_logs)
            raise

    def _populate_lb(self, loadbalancers, listener_args, pool_args,
                     member_args, hm_args):
        """Populate a loadbalancer with listener, pool and members.

        """
        for lb in loadbalancers:
            self.octavia.wait_for_loadbalancer_prov_status(lb)
            listener = self.octavia.listener_create(
                json={
                    "listener": dict(
                        loadbalancer_id=lb["id"],
                        **listener_args)})

            self.octavia.wait_for_loadbalancer_prov_status(lb)
            pool = self.octavia.pool_create(
                lb_id=lb["id"],
                listener_id=listener["listener"]["id"],
                **pool_args)

            for address, ssh in self._members:
                self.octavia.wait_for_loadbalancer_prov_status(lb)
                self.octavia.member_create(
                    pool_id=pool["id"],
                    json={"member": dict(
                        address=address,
                        protocol_port=member_args["port"])})

            self.octavia.wait_for_loadbalancer_prov_status(lb)
            self.octavia.health_monitor_create(
                json={"healthmonitor": dict(
                    pool_id=pool["id"],
                    **hm_args)})

    def _prepare_backend(self, **kwargs):
        """Test the connectivity of the LBs

        """
        create_args = kwargs.copy()
        try:
            create_args.pop("port")
        except KeyError:
            pass
        self._run_command_async(port=22, is_backend=True, **create_args)

    def _test_lb(self, loadbalancers, test_command, timeout=60, **kwargs):
        """Test the connectivity of the LBs

        """
        create_args = kwargs.copy()
        try:
            create_args.pop("command")
            create_args.pop("port")
        except KeyError:
            pass

        for lb in loadbalancers:
            cmd = []
            if isinstance(test_command, list):
                for c in test_command:
                    cmd.append(c.format(lb["vip_address"]))
            else:
                cmd.append(test_command.format(lb["vip_address"]))
            ssh = self._run_command_async(port=22,
                                          command=cmd,
                                          is_backend=False,
                                          **create_args)
            start_time = time.time()
            LOG.debug("session: %s" % ssh.session)
            while True:
                r, w, e = select.select([ssh.session], [], [ssh.session], 1)
                if ssh.session.recv_ready():
                    data = ssh.session.recv(4096)
                    LOG.debug("stdout: %r" % data)
                    continue
                if ssh.session.recv_stderr_ready():
                    stderr_data = ssh.session.recv_stderr(4096)
                    LOG.debug("stderr: %r" % stderr_data)
                    continue
                if ssh.session.exit_status_ready():
                    LOG.debug("exit status ready")
                    break

                if timeout and (time.time() - timeout) > start_time:
                    args = {"cmd": cmd}
                    raise exceptions.SSHTimeout("Timeout executing "
                                                "test_command: %s"
                                                % args["cmd"])
                if e:
                    LOG.info(e)

            code = 255
            if ssh.session is not None:
                code = ssh.session.recv_exit_status()

            ssh.close()
            if code != 0:
                raise Exception("test_command exited with %d instead of "
                                "exit code 0!" % code)

    def _cleanup(self):
        for address, ssh in self._members:
            try:
                ssh.close()
            except AttributeError:
                pass


@types.convert(image={"type": "glance_image"},
               flavor={"type": "nova_flavor"})
@validation.add("image_valid_on_flavor", flavor_param="flavor",
                image_param="image", fail_on_404_image=False)
@validation.add("required_services", services=[consts.Service.NOVA,
                                               consts.Service.CINDER,
                                               consts.Service.OCTAVIA])
@validation.add("required_platform", platform="openstack", users=True)
@scenario.configure(context={"cleanup@openstack": ["nova", "octavia"],
                             "keypair@openstack": {},
                             "allow_ssh@openstack": None},
                    name="Octavia.cern_create_failover_delete_full_lb",
                    platform="openstack")
class CreateFailoverFullLB(BaseFullLB):

    def run(self, flavor, image,
            member_args, listener_args, pool_args, hm_args, test_command,
            admin_state=True, **kwargs):
        """Create a loadbalancer per each subnet and then list loadbalancers.

        :param admin_state: The administrative state of the loadbalancer,
            which is up(true) or down(false)
        :param kwargs: All other arguments for the loadbalancers
        """
        try:
            loadbalancers = self._create_load_balancer_every_flavor(
                admin_state=admin_state, **kwargs)
            self._prepare_backend(flavor=flavor, image=image, **member_args)
            self._populate_lb(loadbalancers, listener_args, pool_args,
                              member_args, hm_args)
            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)

            self._test_lb(loadbalancers, test_command,
                          flavor=flavor, image=image, **member_args)

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
                self.octavia.load_balancer_failover(
                    loadbalancer["id"])

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)

            self._test_lb(loadbalancers, test_command,
                          flavor=flavor, image=image, **member_args)

            for loadbalancer in loadbalancers:
                self.octavia.wait_for_loadbalancer_prov_status(loadbalancer)
                self.octavia.load_balancer_delete(
                    loadbalancer["id"], cascade=True)
        except Exception:
            raise
        finally:
            self._cleanup()
